library ieee;
use ieee.std_logic_1164.all;

entity signext is
    port (
        a : in std_logic_vector(31 downto 0);
        y : out std_logic_vector(63 downto 0)
    );
end entity;

architecture behavior of signext is
begin
    process (a) 
    begin
        case a(31 downto 21) is
            when "111110000X0" =>
                if (a(20) = '1') then
                    y <= (x"FFFF_FFFF_FFFF_FFFF" AND a(20 downto 12));
                elsif (a(20) = '0') then
                    y <= (x"0000_0000_0000_0000" OR a(20 downto 12));
                end if;
            when "10110100XXX" =>
                if (a(23) = '1') then
                    y <= (x"FFFF_FFFF_FFFF_FFFF" AND a(23 downto 5));
                elsif (a(23) = '0') then
                    y <= (x"0000_0000_0000_0000" OR a(23 downto 5));
                end if;
            when others => null;
        end case;
    end process;
end architecture;