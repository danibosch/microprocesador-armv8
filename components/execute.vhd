library ieee;
use ieee.std_logic_1164.all;

entity execute is
    generic (N: integer := 64);
    port (
        aluSrc : in std_logic;
        aluControl : in std_logic_vector(3 downto 0);
        PC_E, signImm_E, readData1_E, readData2_E : in std_logic_vector(N-1 downto 0);
        PCBranch_E, aluResult_E, writeData_E : out std_logic_vector(N-1 downto 0);
        zero_E : out std_logic
    );
end entity;