library ieee;
use ieee.std_logic_1164.all;

entity maindec is
    port (
        op : in std_logic_vector(63 downto 0);
		  Reg2Loc, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch : out std_logic;
        ALUOp : out std_logic_vector(1 downto 0)
    );
end entity;

