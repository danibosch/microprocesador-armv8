library ieee;
use ieee.std_logic_1164.all;

entity regfile is
    port (
        we3, clk : in std_logic;
        ra1, ra2, wa3 : in std_logic_vector (4 downto 0);
        wd3 : in std_logic_vector (63 downto 0);
        rd1, rd2 : out std_logic_vector (63 downto 0)
    );
end entity;

--architecture behavior of regfile is
--begin

--end architecture;