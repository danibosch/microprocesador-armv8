library ieee;
use ieee.std_logic_1164.all;

entity fetch is
    generic (N: integer := 64);
    port (
        clk, reset : in std_logic;
        imem_addr_F : out std_logic_vector(5 downto 0);
        PCBranch_F : out std_logic_vector(N-1 downto 0);
        PCSrc_F : out std_logic
    );
end entity;